package com.css.lykio.cr.controllers;

import com.css.lykio.cr.entities.Author;
import com.css.lykio.cr.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthorController {

    @Autowired
    AuthorRepository authorRepository;

    @RequestMapping("/authors")
    public Iterable<Author> getAuthors() {
        return authorRepository.findAll();
    }
}