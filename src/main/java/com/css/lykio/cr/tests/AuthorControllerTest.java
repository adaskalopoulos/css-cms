package com.css.lykio.cr.tests;

import com.css.lykio.cr.Application;
import com.css.lykio.cr.entities.Author;
import com.css.lykio.cr.repositories.AuthorRepository;
import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class AuthorControllerTest {

    @Autowired
    AuthorRepository repository;

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {
        repository.deleteAll();
        repository.save(new Author("Jack"));
        repository.save(new Author("Chloe"));
        repository.save(new Author("Kim"));
        repository.save(new Author("David"));
        repository.save(new Author("Michelle"));
        RestAssured.port = port;
    }

    @Test
    public void canFetchAll() {
        when().get("/authors")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("size()", equalTo(5));
    }
}