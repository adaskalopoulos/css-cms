package com.css.lykio.cr.repositories;

import com.css.lykio.cr.entities.Author;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {

    List<Author> findByName(@Param("name") String name);
}